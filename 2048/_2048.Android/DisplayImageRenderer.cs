﻿using System;
using Android.Views;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using _2048;
using _2048.Droid;




[assembly: ExportRenderer(typeof(SwipeLayer), typeof(DisplayImageRenderer))]

namespace _2048.Droid
{
    class DisplayImageRenderer : ImageRenderer
    {
        public float X1 { get; set; }
        public float X2 { get; set; }
        public float Y1 { get; set; }
        public float Y2 { get; set; }

        private const float moveThrestHold = 50;

        bool moved = true;

        public SwipeLayer swipeLayer { get; set; }

        public override bool OnTouchEvent(MotionEvent e)
        {
            X2 = 0;
            Y2 = 0;

            if (e.ActionMasked == MotionEventActions.Down)
            {
                X1 = e.GetX();
                Y1 = e.GetY();

                moved = false;



                return true;
            }
            else if (e.ActionMasked == MotionEventActions.Up)
            {
                X2 = e.GetX();
                Y2 = e.GetY();


                var xChange = X1 - X2;
                var yChange = Y1 - Y2;

                var xChangeSize = Math.Abs(xChange);
                var yChangeSize = Math.Abs(yChange);
                if (!moved)
                    if (xChangeSize > moveThrestHold || yChangeSize > moveThrestHold)
                        if (xChangeSize > yChangeSize)
                        {
                            // horizontal
                            if (X1 > X2)
                            {
                                // left
                                swipeLayer.RaiseSwipedLeft();
                                moved = true;
                            }
                            else
                            {
                                // right
                                swipeLayer.RaiseSwipedRight();
                                moved = true;
                            }

                        }
                        else
                        {
                            // vertical
                            if (Y1 > Y2)
                            {
                                // up
                                swipeLayer.RaiseSwipedUp();
                                moved = true;
                            }
                            else
                            {
                                // down
                                swipeLayer.RaiseSwipedDown();
                                moved = true;
                            }

                        }
            }

            return base.OnTouchEvent(e);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Image> ev)
        {
            base.OnElementChanged(ev);

            swipeLayer = (SwipeLayer)ev.NewElement;
        }

    }
}