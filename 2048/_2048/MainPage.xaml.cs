﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace _2048
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void PlayClicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new GameScene());
        }
    }
}
