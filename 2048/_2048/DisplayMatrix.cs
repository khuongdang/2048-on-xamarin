﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



using Xamarin.Forms;

namespace _2048
{
    public class DisplayMatrix : Grid
    {
        int x;
        int y;
        private GameMatrix Matrix;

        Color[] Arr_Color = new Color[12] { Color.Tan, Color.WhiteSmoke, Color.Gray, Color.LawnGreen ,
                                            Color.Green, Color.GreenYellow, Color.YellowGreen, Color.LightYellow,
                                            Color.Yellow, Color.Cyan, Color.LightBlue, Color.Blue };


        public DisplayMatrix(int _x = 4, int _y = 4)
        {
            x = _x;
            y = _y;
            Matrix = new GameMatrix(x, y);
            Init();
        }

        private void Init()
        {
            this.Padding = 10;
            this.HorizontalOptions = LayoutOptions.FillAndExpand;
            this.VerticalOptions = LayoutOptions.FillAndExpand;
            this.BackgroundColor = Color.Brown;         

            CreateGrid();
            UpdateMatrix();
        }

        private void CreateGrid()
        {
            for (int i = 0; i < 4; i++)
            {
                ColumnDefinition column = new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) };
                RowDefinition row = new RowDefinition { Height = new GridLength(1, GridUnitType.Star) };

                this.RowDefinitions.Add(row);
                this.ColumnDefinitions.Add(column);
            }
        }

        public int MoveUp()
        {
            int result = Matrix.MoveUp();
            UpdateMatrix();
            if (result == 1) Navigation.PushAsync(new GameOver());
            return result;
        }

        public int MoveDown()
        {

            int result = Matrix.MoveDown();
            UpdateMatrix();
            if (result == 1) Navigation.PushAsync(new GameOver());
            return result;
        }
        public int MoveLeft()
        {
            int result = Matrix.MoveLeft();
            UpdateMatrix();
            if (result == 1) Navigation.PushAsync(new GameOver());
            return result;
        }
        public int MoveRight()
        {
            int result = Matrix.MoveRight();
            UpdateMatrix();
            if (result == 1) Navigation.PushAsync(new GameOver());
            return result;
        }

        public void UpdateMatrix()
        {
            this.Children.Clear();
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    Label Title = new Label
                    {
                        Text = Matrix.TitleValue(i, j).ToString(),
                        TextColor = Color.Blue,
                        BackgroundColor = Arr_Color[ Matrix.TitleColorIndex(i,j) ],
                        HorizontalTextAlignment = TextAlignment.Center,
                        VerticalTextAlignment = TextAlignment.Center
                    };
                    this.Children.Add(Title, i, i + 1, j, j + 1);
                }
            }
        }




    }
}
