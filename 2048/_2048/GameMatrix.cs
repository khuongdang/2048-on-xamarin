﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;

namespace _2048
{
    class GameMatrix
    {
        private bool moved;

        private int x;
        private int y;
        private int[,] Matrix;
        public GameMatrix(int _x = 4, int _y = 4 )
        {
            x = _x;
            y = _y;
            Matrix = new int[x, y];
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    Matrix[i, j] = 0;
                }
            }

            CreateRandom();
        }

        public int TitleValue(int _x, int _y)
        {
            return Matrix[_x, _y] << 1;
        }

        public int TitleColorIndex(int _x, int _y)
        {
            int num = 0;
            int temp = Matrix[_x, _y];
            while (temp > 0)
            {
                num++;
                temp >>= 1;
            }
            return num;
        }

        private int CreateRandom(){
            if (!IsMovable()) return 1;

            moved = false;

            Random rd = new Random( Guid.NewGuid().GetHashCode() );
            int x1 = rd.Next(x);
            int y1 = rd.Next(y);
            int x2 = rd.Next(x);
            int y2 = rd.Next(y);

            while (Matrix[x1, y1] != 0)
            {
                x1 = rd.Next(x);
                y1 = rd.Next(y);
            }

            while ( (x1 == x2 && y1 == y2) || Matrix[x2, y2] != 0)
            {
                x2 = rd.Next(x);
                y2 = rd.Next(y);
            }

            Matrix[x1, y1] = 1;
            Matrix[x2, y2] = 1;

            return 0;
        }

        private bool IsMovable()
        {

            for (int i = 0; i < x-1; i++)
            {
                for (int j = 0; j < y-1; j++)
                {
                    if (Matrix[i, j] == 0)
                        return true;
                    if (Matrix[i, j] == Matrix[i, j + 1])
                        return true;
                    if (Matrix[i, j] == Matrix[i+1, j])
                        return true;
                }
                if (Matrix[i, y - 1] == 0)
                    return true;
                if (Matrix[i, y - 1] == Matrix[i + 1, y - 1])
                    return true;
            }

            for (int j = 0; j < y - 1; j++)
            {
                if (Matrix[x-1, j] == 0)
                    return true;
                if (Matrix[x-1, j] == Matrix[x-1, j + 1])
                    return true;
            }
            if (Matrix[x-1, y - 1] == 0)
                return true;

            return false;
        }

        public int MoveUp(){
           
            for (int i = 0; i < x; i++)
            {
                moved = StackUp(i);
                int pos = 0;
                while(pos < x-1)
                {
                    if(Matrix[i,pos] == Matrix[i,pos+1])
                    {
                        Matrix[i, pos] <<= 1;                      
                        Matrix[i, pos+1] = 0;
                        StackUp(i);
                        moved = true;
                    }
                    pos++;
                }
            }

            if (moved)
                return CreateRandom();
            return 0;
        }

        public int MoveDown()
        {
            for (int i = 0; i < x; i++)
            {
                moved = StackDown(i);
                int pos = x - 1;
                while (pos > 0)
                {
                    if (Matrix[i, pos] == Matrix[i, pos - 1])
                    {
                        Matrix[i, pos] <<= 1;
                        Matrix[i, pos - 1] = 0;
                        StackDown(i);
                        moved = true;
                    }
                    pos--;
                }
            }

            if (moved)
                return CreateRandom();
            return 0;
        }

        public int MoveLeft()
        {

            for (int j = 0; j < x; j++)
            {

                moved = StackLeft(j);

                int pos = 0;
                while (pos < y - 1)
                {
                    if (Matrix[pos, j] == Matrix[pos +1, j])
                    {
                        Matrix[pos,j] <<= 1;
                        Matrix[pos + 1,j] = 0;
                        StackLeft(j);
                        moved = true;
                    }
                    pos++;
                }           
            }

            if (moved)
                return CreateRandom();
            return 0;
        }

        public int MoveRight()
        {

            for (int j = 0; j < x; j++)
            {
                moved = StackRight(j);

                int pos = y-1;
                while (pos > 0)
                {
                    if (Matrix[pos, j] == Matrix[pos - 1, j])
                    {
                        Matrix[pos, j] <<= 1;
                        Matrix[pos - 1, j] = 0;
                        StackRight(j);
                        moved = true;
                    }
                    pos--;
                }
            }

            if (moved)
                return CreateRandom();
            return 0;
        }

        private bool StackUp(int i)
        {
            bool _moved = false;

            int pos = 0;
            for(int j = 0; j < y; j++)
            {
                if(Matrix[i,j] != 0)
                {
                    Matrix[i, pos] = Matrix[i, j];
                    pos++;
                    _moved = true;
                }
            }
            while (pos<y)
            {            
                Matrix[i, pos] = 0;
                pos++;
            }

            return _moved;
        }

        private bool StackDown(int i)
        {
            bool _moved = false;

            int pos = y-1;
            for (int j = y-1; j >= 0; j--)
            {
                if (Matrix[i, j] != 0)
                {
                    Matrix[i, pos] = Matrix[i, j];
                    pos--;
                    _moved = true;
                }
            }
            while (pos >= 0)
            {
                Matrix[i, pos] = 0;
                pos--;
            }

            return _moved;
        }

        private bool StackLeft(int j)
        {
            bool _moved = false;

            int pos = 0;
            for (int i = 0; i < x; i++)
            {
                if (Matrix[i, j] != 0)
                {
                    Matrix[pos, j] = Matrix[i, j];
                    pos++;
                    _moved = true;
                }
            }
            while (pos < x)
            {
                Matrix[pos, j] = 0;
                pos++;
            }

            return _moved;
        }

        private bool StackRight(int j)
        {
            bool _moved = false;

            int pos = x-1;
            for (int i = x-1; i >=0 ; i--)
            {
                if (Matrix[i, j] != 0)
                {
                    Matrix[pos, j] = Matrix[i, j];
                    pos--;
                    _moved = true;
                }
            }
            while (pos >=0)
            {
                Matrix[pos, j] = 0;
                pos--;
            }

            return _moved;
        }

    }
}
