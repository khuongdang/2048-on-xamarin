﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace _2048
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GameScene : ContentPage
    {
        public GameScene()
        {
            InitializeComponent();

            GameMatrix matrix = new GameMatrix(4, 4);

            // Create Lable
            Label GameLabel = new Label
            {
                Text = "2048",
                HorizontalOptions = LayoutOptions.Center,
                FontSize = 20
            };


            DisplayMatrix displayMatrix = new DisplayMatrix();

            Label Test = new Label { HorizontalOptions = LayoutOptions.Center, Text="dmm"};

            SwipeLayer displayImage = new SwipeLayer(ref Test);

            displayImage.SwipedUp += (sender, args) => { displayMatrix.MoveUp(); };
            displayImage.SwipedDown += (sender, args) => { displayMatrix.MoveDown(); };
            displayImage.SwipedLeft += (sender, args) => { displayMatrix.MoveLeft(); };
            displayImage.SwipedRight += (sender, args) => { displayMatrix.MoveRight(); };

            RelativeLayout relativeLayout = new RelativeLayout {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
            };

 
            relativeLayout.Children.Add(displayMatrix,
                                        Constraint.RelativeToParent((parent) => { return parent.X; }),
                                        Constraint.RelativeToParent((parent) => { return parent.Y; }),
                                        Constraint.RelativeToParent((parent) => { return parent.Width; }),
                                        Constraint.RelativeToParent((parent) => { return parent.Height; })
                                        );

            relativeLayout.Children.Add(displayImage,
                             Constraint.RelativeToParent((parent) => { return parent.X; }),
                             Constraint.RelativeToParent((parent) => { return parent.Y; }),
                             Constraint.RelativeToParent((parent) => { return parent.Width; }),
                             Constraint.RelativeToParent((parent) => { return parent.Height; })
                             );



            this.Content = new StackLayout
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Children =
                {
                    GameLabel,
                    relativeLayout
                }
            };
        }
    }
}